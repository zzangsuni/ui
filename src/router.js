import Vue from 'vue';
import Router from 'vue-router';

// import Container from '@/views/container.vue';
import ButtonView from '@/views/button_view.vue';
import CheckboxView from '@/views/checkbox_view.vue';
import ScopedView from '@/views/scoped_view.vue';
import SlotView from '@/views/slot_view.vue';
import StyleComponents from '@/views/stylecmp_view.vue';
import Puzzle from '@/views/puzzle_view.vue';

Vue.use(Router);

export default new Router({
  mode:'history',
  routes:[
    // {path:'/', component:Container},
    {path:'/button', component:ButtonView},
    {path:'/checkbox', component:CheckboxView},
    {path:'/scoped', component:ScopedView},
    {path:'/slot', component:SlotView},
    {path:'/stylecmp', component:StyleComponents},
    {path:'/puzzle', component:Puzzle},
  ]
});